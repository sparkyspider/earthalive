#EarthAlive Platform
## Tech platform offering 'positive change' projects online exposure via collaborative and online publishing tools.
EarthAlive Platform is a project collaboration system for positive change projects. A similar concept to KickStarter, EarthAlivePlatform allows people to create projects on the platform, and then allows others to come in and support those projects via volunteering skills or resources. EarthAlive Platform also hosts various tools to offer these projects exposure online - such as email marketing, Facebook sharing, fundraising, content management, hosting, etc.

##Earthalive.org
The development team behind [earthalive.org](www.earthalive.org) have open-sourced the platform.